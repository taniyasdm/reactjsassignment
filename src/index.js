import React from 'react';
import ReactDOM from 'react-dom';
import { Route, Switch } from 'react-router-dom';
import { HashRouter } from 'react-router-dom'
import { PrivateRoute } from './routes/PrivateRoute';
import  { HomePage }  from './components/HomePage';
import { cartPage } from './components/cartPage';
import { LoginPage } from './components/LoginPage';
import { BusinessPage } from './components/BusinessPage';
import { ProductsPage } from './components/ProductsPage';
import { RegisterPage } from './components/RegisterPage';
import './assets/scss/style.css';



ReactDOM.render(
  <HashRouter>

    <Switch>
      <Route exact path="/" component={LoginPage} />
      <Route path="/dashboard" component={HomePage} />
      <Route path="/businesses" component={BusinessPage} />
      <Route path="/products" component={ProductsPage} />
      <Route path="/cart" component={cartPage} />
      <Route path="/register" component={RegisterPage} />

    </Switch>

  </HashRouter>
  , document.getElementById('root')); 
