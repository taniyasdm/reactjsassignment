import React from 'react';
import Header from '../header/header.jsx';
import Sidebar from '../sidebar/sidebar.jsx';
import { Link } from 'react-router-dom';
import Footer from '../footer/footer.jsx';
import axios from 'axios';
import {
    Card,
    CardImg,
    CardText,
    CardBody,
    CardTitle,
    Button,
    Row,
    Col
} from 'reactstrap';

export class BusinessPage extends React.Component {
    state = {
        categories: [],
        subcats: [],
        isLoading: true
    }
    constructor(props) {
        super(props);
        this.getCategories();
    }

    getCategories = () => {
        let webData = JSON.parse(localStorage.getItem('itemValue'))
        const url = `https://mean.stagingsdei.com:6047/merchant/pagination?perPage=10&page=1&filter=&sortBy=email&orderBy=asc&catId=` + webData.categoryId;
        axios.get(url)
            .then(result => {
                this.setState({ categories: result.data.data.data, isLoading: false });
            })
            .catch(error => {
                this.setState({ isLoading: false });
            });
    }

    saveDataToLocalStorage = (item) => {
        let objToSet = {
            categoryName: item.title,
            categoryId: item._id,
            merchantName: '',
            merchantId: '',
            subCatName: '',
            subCatId: '',
            description: item.description
        }
        localStorage.setItem('itemValue', JSON.stringify(objToSet));
    }


    render() {
        return (
            <div
                id="main-wrapper"
                data-theme="light"
                data-layout="vertical"
                data-sidebartype="full"
                data-sidebar-position="fixed"
                data-header-position="fixed"
                data-boxed-layout="full"
            >
                <Header />
                <Sidebar />
                <div className="page-wrapper d-block">
                    <div className="page-content container-fluid">
                        <div >
                            <Row>
                                {this.state.categories.length > 0 ? (
                                    this.state.categories.map((item, index) => {
                                        return <Col xs="12" md="4" key={index}>
                                            <Card key={index} className="merchants-card">
                                                <CardImg top width="100%" src={item.merchant_image} />
                                                <CardBody>
                                                    <CardTitle>{item.name}</CardTitle>
                                                    <CardText>{item.description}</CardText>
                                                    <Button >
                                                        <Link to={`/products`}>
                                    <span onClick={() => this.saveDataToLocalStorage(item)}>Select {item.name}</span>
                                                        </Link></Button>
                                                </CardBody>
                                            </Card>
                                        </Col>
                                    })
                                ) : (<div> No results </div>)
                                }
                            </Row>
                        </div>

                    </div>
                    <Footer />
                </div>
            </div>
        );
    }
}

