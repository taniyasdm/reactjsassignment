import React from 'react';
import Header from '../header/header.jsx';
import Sidebar from '../sidebar/sidebar.jsx';
import { Link } from 'react-router-dom';
import Footer from '../footer/footer.jsx';
import axios from 'axios';
import {
    Card,
    CardImg,
    CardText,
    CardBody,
    CardTitle,
    Button,
    Row,
    Col
} from 'reactstrap';

export class HomePage extends React.Component {
    state = {
        categories: [],
        subcats: [],
        isLoading: true
    }
    constructor(props) {
        super(props);
        this.getCategories();
    }

    getCategories = () => {
        const url = 'https://mean.stagingsdei.com:6047/category/pagination?perPage=10&page=1&filter=&sortBy=title&orderBy=asc';
        axios.get(url)
            .then(result => {
                this.setState({ categories: result.data.data.data, isLoading: false });
            })
            .catch(error => {
                this.setState({ isLoading: false });
            });
    }

    saveDataToLocalStorage = (item) => {
        let objToSet = {
            categoryName: item.title,
            categoryId: item._id,
            merchantName: '',
            merchantId: '',
            subCatName: '',
            subCatId: '',
            description: item.description
        }
        localStorage.setItem('itemValue', JSON.stringify(objToSet));
    }


    render() {
        return (
            <div
                id="main-wrapper"
                data-theme="light"
                data-layout="vertical"
                data-sidebartype="full"
                data-sidebar-position="fixed"
                data-header-position="fixed"
                data-boxed-layout="full"
            >
                <Header />
                <Sidebar />
                <div className="page-wrapper d-block">
                    <div className="page-content container-fluid">
                        <div>
                            <Row>
                                {this.state.categories.length > 0 ? (
                                    this.state.categories.map((item, index) => {
                                        return <Col xs="12" md="4" key={index}>
                                            <Card key={index}>
                                                <CardImg top width="100%" src={item.icon} />
                                                <CardBody>
                                                    <CardTitle>{item.title}</CardTitle>
                                                    <CardText>{item.description}</CardText>
                                                    <Button  onClick={() => this.saveDataToLocalStorage(item)}>
                                                        <Link to={`/businesses`}>
                                                            <span >Select {item.title}</span>
                                                        </Link></Button>
                                                </CardBody>
                                            </Card>
                                        </Col>
                                    })
                                ) : (<div> No results </div>)
                                }
                            </Row>
                        </div>

                    </div>
                    <Footer />
                </div>
            </div>
        );
    }
}

