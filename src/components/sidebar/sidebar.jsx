import React from 'react';
import { NavLink } from 'react-router-dom';
import { Nav } from 'reactstrap';
import PerfectScrollbar from 'react-perfect-scrollbar'

const Sidebar = (props) => {

    /*--------------------------------------------------------------------------------*/
    /*To Expand SITE_LOGO With Sidebar-Menu on Hover                                  */
    /*--------------------------------------------------------------------------------*/
    const expandLogo = () => {
        document.getElementById("logobg").classList.toggle("expand-logo");
    }
    /*--------------------------------------------------------------------------------*/
    /*Verifies if routeName is the one active (in browser input)                      */
    /*--------------------------------------------------------------------------------*/

    const activeRoute = (routeName) => {
        // return props.location.pathname.indexOf(routeName) > -1 ? 'selected' : '';
    }

    return (
        <aside className="left-sidebar" id="sidebarbg" data-sidebarbg="skin6" onMouseEnter={expandLogo.bind(null)} onMouseLeave={expandLogo.bind(null)}>
            <div className="scroll-sidebar">
                <PerfectScrollbar className="sidebar-nav">
                    {/*--------------------------------------------------------------------------------*/}
                    {/* Sidebar Menus will go here                                                */}
                    {/*--------------------------------------------------------------------------------*/}
                    <Nav id="sidebarnav">

                        <li className={activeRoute('/dashboard') + ' sidebar-item'}>
                            <NavLink to="/dashboard" className="sidebar-link" activeClassName="active">
                                <i className="mdi mdi-credit-card-multiple" />
                                <span className="hide-menu">Categories</span>
                            </NavLink>
                        </li>
                        {/* <li className={activeRoute('/cart') + ' sidebar-item'}>
                            <NavLink to="/cart" className="sidebar-link" activeClassName="active">
                                <i className="mdi mdi-arrange-send-backward" />
                                <span className="hide-menu">My Cart</span>
                            </NavLink>
                        </li> */}
                        <li className={activeRoute('/') + ' sidebar-item'}>
                            <NavLink to="/" className="sidebar-link" activeClassName="active">
                                <i className="ti-loop" />
                                <span className="hide-menu">Logout</span>
                            </NavLink>
                        </li>

                    </Nav>
                </PerfectScrollbar>
            </div>
        </aside>
    );
}
export default Sidebar;
