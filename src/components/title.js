import React from "react";

const TitleComponent = ({ title }) => {
    var defaultTitle = '⚛️ app';
    return (
        <div>
            <meta charSet="utf-8" />
            <h2>{title ? title : defaultTitle}</h2>
        </div>
    );
};

export default TitleComponent;
