import React from 'react';
import Header from '../header/header.jsx';
import Sidebar from '../sidebar/sidebar.jsx';
import Footer from '../footer/footer.jsx';
import {
    Card,
    CardBody,
    CardTitle,
    CardSubtitle,
    Table
} from 'reactstrap';


export class cartPage extends React.Component {
    state = {
        cartItems: []
    }
    // constructor(props) {
    //     super(props);
    // }

    render() {
        const mycart = JSON.parse(localStorage.getItem('cartItems'));
        return (
            <div
                id="main-wrapper"
                data-theme="light"
                data-layout="vertical"
                data-sidebartype="full"
                data-sidebar-position="fixed"
                data-header-position="fixed"
                data-boxed-layout="full"
            >
                <Header />
                <Sidebar />
                <div className="page-wrapper d-block">
                    <div className="page-content container-fluid">
                        <div>
                            <Card>
                                <CardBody>
                                    <div className="d-flex align-items-center">
                                        <div>
                                            <CardTitle>My Cart</CardTitle>
                                            <CardSubtitle>Products added in Cart</CardSubtitle>
                                        </div>

                                    </div>
                                    <Table className="no-wrap v-middle" responsive>
                                        <thead>
                                            <tr className="border-0">
                                                <th className="border-0">Name</th>
                                                <th className="border-0">Type</th>

                                                <th className="border-0">Qty</th>
                                                <th className="border-0">Price</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {mycart.length > 0 ? (
                                                mycart.map((item, index) => {
                                                    return <tr>
                                                        <td>
                                                            <div className="d-flex no-block align-items-center">
                                                                <div className="mr-2"><img src={item.icon} alt="user" className="rounded-circle" width="45" /></div>
                                                                <div className="mr-4">
                                                                    <h5 className="mb-0 font-16 font-medium">{item.title}</h5>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div className="mr-4">
                                                                <h5 className="mb-0 font-16 font-medium">{item.variation ? item.variation[0].title : '-'}</h5>
                                                            </div>
                                                        </td>
                                                        <td>1</td>
                                                        <td className="blue-grey-text  text-darken-4 font-medium">
                                                            <i className="fa fa-circle text-orange" id="tlp1"></i>
                                                           $17
                                                           </td>
                                                    </tr>
                                                })
                                            ) : ''

                                            }
                                        </tbody>
                                    </Table>
                                </CardBody>
                            </Card ></div>

                    </div>
                    <Footer />
                </div>
            </div>
        );
    }
}

